/*
Comparación entre N valores de cualquier tipo: "==", "!="
Comparación entre N números: "<", "<=", ">", ">="
Operaciones numéricas unarias: "NEGATE"
Operaciones numéricas binarias: "-", "/"
Operaciones numéricas de N valores: "+", "*", "MIN", "MAX", "AVERAGE", "STDDEV"
Operaciones booleanas unarias: "NOT"
Operaciones booleanas de N valores: "AND", "OR"
*/

const conditionEvaluator = (conditionArgs) => {
  return new Promise((resolve, reject) => {
    const shouldExecute = Object.keys(conditionArgs).reduce((acumulator, currentCondition) => { acumulator && executeCondition(currentCondition) });
    if(!shouldExecute)
      reject('fail');
    resolve('success');
  })
};

const { condition, action } = rules;
const conditionEvaluator = conditionEvaluator(condition);
conditionEvaluator.then(() => onSuccess(action))
                  .then(() => onFailure())
                  .catch((errors) => errorHandler(errors));