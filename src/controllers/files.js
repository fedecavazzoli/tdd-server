'use strict';

const { files } = require('../services/index');
const STATUS_CODES = require('../utils/status-codes.json');

const upload = async (req, res) => {

	const { body: file, session: { _id } } = req;

	try {
		const fileCreated = await files.createFile(file, _id);
		return res.status(STATUS_CODES.OK).send({ fileCreated });
	} catch({ message }) {
		return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message });
	}
};

const getTree = async (req, res) => {

	try {
		const { session: { _id } } = req;
		const tree = await files.getTree(_id);
		return res.status(STATUS_CODES.OK).send(tree);
	} catch({ message }) {
		return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message });
	}
};

const getFilesAndDirectories = async (req, res) => {

	const { query: { path = '' }, session: { _id } } = req;

	try {
		const filesFound = await files.getFiles(path, _id); // directories and files
		return res.status(STATUS_CODES.OK).send({ filesFound });
	} catch({ message }) {
		return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message });
	}
};

const getFile = async (req, res) => {

	const { query: { path }, session: { _id } } = req;

	try {

		const file = await files.getFile(path, _id);

		if(!file)
			return res.status(STATUS_CODES.NOT_FOUND).send({ message: `File with path ${path} not found` });

		return res.status(STATUS_CODES.OK).send(file);
	} catch({ message }) {
		return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message });
	}
};

const deleteFile = async (req, res) => {

	const { query: { path }, session: { _id } } = req;

	try {

		const fileDeleted = await files.deleteFile(path, _id);

		if(!fileDeleted)
			return res.status(STATUS_CODES.NOT_FOUND).send({ message: `File with path ${path} not found` });

		return res.status(STATUS_CODES.OK).send({ message: `File with path ${path} was deleted` });
	} catch({ message }) {
		return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message });
	}
};

const deleteDirectory = async (req, res) => {

	const { query: { path }, session: { _id } } = req;

	try {

		const children = await files.getChildren(path, _id);

		if(children.length)
			return res.status(STATUS_CODES.BAD_REQUEST).send({ message: `Directory with path ${path} not empty` });

		const directoryDeleted = await files.deleteFile(path, _id);

		if(!directoryDeleted)
			return res.status(STATUS_CODES.NOT_FOUND).send({ message: `Directory with path ${path} not found` });

		// FALTA BORRAR EL ARCHIVO EN LOS USUARIOS A LOS QUE SE LES COMPARTIO

		return res.status(STATUS_CODES.OK).send({ message: `Directory with path ${path} was deleted` });
	} catch({ message }) {
		return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message });
	}
};

module.exports = {
	upload,
	getFile,
	getFilesAndDirectories,
	getTree,
	deleteFile,
	deleteDirectory
};
