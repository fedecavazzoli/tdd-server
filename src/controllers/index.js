'use strict';

const users = require('./users');
const files = require('./files');
const pruebas = require('./prueba');
const rules = require('./rules');
const wallets = require('./wallets');

module.exports = {
	users,
	files,
	pruebas,
	rules,
	wallets
};
