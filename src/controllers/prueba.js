'use strict';

const STATUS_CODES = require('../utils/status-codes.json');

const { rules } = require('../services/index.js');
const json = require('../../jsonprueba.json');

const prueba = async (req, res) => {
	const { requiredVariables, rules: rule, users } = json;
	const { condition, action, name } = rule;
	await rules.createRule({
		name, condition, action, requiredVariables, users
	});
	const ruleFinded = await rules.getRuleByName(name);
	const evaluation = await rules.evaluateRule(ruleFinded);
	return res.status(STATUS_CODES.OK).send(evaluation);
};

module.exports = { prueba };
