'use strict';

const STATUS_CODES = require('../utils/status-codes.json');
const { rules } = require('../services/index.js');


const create = async (req, res) => {
	const { requiredVariables, rules: rule } = req.body;
	const { condition, action, name } = rule;
	const currentUser = req.session._id;
	const usersList = [];
	usersList.push(currentUser);


	try {
		const persistedRule = await rules.createRule({
			name, condition, action, requiredVariables, users: usersList
		});
		return res.status(STATUS_CODES.OK).send({ data: persistedRule });
	} catch(error) {
		return res.status(STATUS_CODES.BAD_REQUEST).send({ error: error.message });
	}
};

const getRules = async (req, res) => {
	const { _id: userId } = req.session;
	try {
		const allRules = await rules.getByUser(userId);
		return res.status(STATUS_CODES.OK).send({ data: allRules });
	} catch(error) {
		console.log(error);
		return res.status(STATUS_CODES.BAD_REQUEST).send({ error: error.message });
	}
};

const getRule = async (req, res) => {
	const { rule_id: ruleId } = req.body;
	if(!ruleId)
		return res.status(STATUS_CODES.NOT_FOUND).send({ error: 'ruleId cant be null' });

	try {
		const rule = await rules.getRuleById(ruleId);
		return res.status(STATUS_CODES.OK).send({ data: rule });
	} catch(error) {
		console.log(error);
		return res.status(STATUS_CODES.BAD_REQUEST).send({ error: error.message });
	}
};

const enable = async (req, res) => {
	const { rule_id: ruleId } = req.params;
	try {
		await rules.updateEnable(ruleId, true);
		const rule = await rules.getRuleById(ruleId);
		return res.status(STATUS_CODES.OK).send({ data: rule });
	} catch(err) { return res.status(STATUS_CODES.BAD_REQUEST).send({ error: err.message }); }
};

const disable = async (req, res) => {
	const { rule_id: ruleId } = req.params;
	try {
		await rules.updateEnable(ruleId, false);
		const rule = await rules.getRuleById(ruleId);
		return res.status(STATUS_CODES.OK).send({ data: rule });
	} catch(err) { return res.status(STATUS_CODES.BAD_REQUEST).send({ error: err.message }); }
};

const fetchCoin = async (req, res) => {
	const { coin } = req.body;
	try {
		const findedRules = await rules.getAllWithCoin(coin);
		return res.status(STATUS_CODES.OK).send({ data: findedRules });
	} catch(err) { return res.status(STATUS_CODES.BAD_REQUEST).send({ error: err.message }); }
};

module.exports = {
	create, getRules, getRule, enable, disable, fetchCoin
};
