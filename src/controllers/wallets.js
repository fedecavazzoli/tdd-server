'use strict';

const { users } = require('../services/index');
const binanceUtils = require('../services/binance/utils');
const STATUS_CODES = require('../utils/status-codes.json');


const setCredentials = async (req, res) => {
	const { _id: userId } = req.session;
	const { apiKey, apiSecret } = req.body;

	try {
		users.createWallet({ id: userId, apiKey, apiSecret });
		const user = await users.getUserById(userId);
		return res.status(STATUS_CODES.OK).send({ data: user });
	} catch(error) { return res.status(STATUS_CODES.OK).send({ error: error.message }); }
};

const buy = async (req, res) => {
	const { _id: userId } = req.session;
	const { wallet } = await users.getUserById(userId);
	const { symbol, amount } = req.body;
	try {
		const response = await binanceUtils.buy({ apiKey: wallet.apiKey, apiSecret: wallet.apiSecret }, { symbol, amount });
		return res.status(STATUS_CODES.OK).send({ message: response });
	} catch(error) { console.log(error); }
};

const sell = async (req, res) => {
	const { _id: userId } = req.session;
	const { wallet } = await users.getUserById(userId);
	const { symbol, amount } = req.body;
	try {
		const response = await binanceUtils.sell({ apiKey: wallet.apiKey, apiSecret: wallet.apiSecret }, { symbol, amount });
		return res.status(STATUS_CODES.OK).send({ message: response });
	} catch(error) { console.log(error); }
};

const getWallet = async (req, res) => {
	const { _id: userId } = req.session;

	const { wallet } = await users.getUserById(userId);
	return res.status(STATUS_CODES.OK).send({ data: wallet });
};

const deleteWallet = async (req, res) => {
	const { _id: userId } = req.session;
	try {
		await users.createWallet({ id: userId, apiKey: '', apiSecret: '' });
		const user = await users.getUserById(userId);
		return res.status(STATUS_CODES.OK).send({ data: user });
	} catch(error) { return res.status(STATUS_CODES.OK).send({ error: error.message }); }
};

module.exports = {
	setCredentials,
	buy,
	sell,
	getWallet,
	deleteWallet
};
