'use strict';

const mongoose = require('mongoose');
const { ObjectId } = require('mongoose').Types;

module.exports = class Model {

	constructor(name, schema) {
		this.model = mongoose.model(name, schema);
	}

	async find(filters = {}) {
		const items = await this.model.find(filters);

		if(!items)
			return [];

		return items.map(({ _doc }) => _doc);
	}

	async findOne(field, value) {
		const { _doc } = await this.model.findOne({ [field]: value });
		return _doc;
	}

	async findBy(field, value) {
		const items = await this.model.find({ [field]: value });

		if(!items || !items.length)
			return [];

		return items.map(({ _doc }) => _doc);
	}

	async create(data) {
		const { _doc } = await this.model.create(data);
		return _doc;
	}

	async update(filters, data) {
		const { ok } = await this.model.updateOne(filters, data);
		return !!ok;
	}

	async remove(filters) {
		return this.model.find(filters)
			.remove()
			.exec();
	}

	async updateVariable(userId, variable) {
		const { name, value } = variable;
		const { n } = await this.model.updateOne({ _id: new ObjectId(userId), 'variables.name': name },
			{ $set: { 'variables.$.value': value } });
		if(n === 0) {
			// eslint-disable-next-line no-shadow
			const { n } = await this.model.updateOne({ _id: new ObjectId(userId) }, { $push: { variables: { name, value } } });
			return (n === 1);
		}
		return n === 1;
	}
};
