'use strict';

const Express = require('express');
const cors = require('cors');

const swaggerUi = require('swagger-ui-express');
const routes = require('./routes/index');

require('dotenv').config();
require('./startup/startup-mongo')();
require('./startup/sockets')();
const options = require('../documentation/options');

const app = new Express();

app.use(cors({
	origin: 'https://grupo0.tecnicasdedisenio.com.ar'
}));


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(options));

app.use('/api', routes);

const port = 8080;

app.listen(port, () => console.log(`App listening on port ${port}`));


module.exports = app;
