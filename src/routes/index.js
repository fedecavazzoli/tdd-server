'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const router = express.Router();

router.use(express.static('public'));
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(morgan('dev'));

const users = require('./users');
const files = require('./files');
const rules = require('./rules');
const wallets = require('./wallets');

router.use('/users', users);
router.use('/files', files);
router.use('/rules', rules);
router.use('/wallets', wallets);

module.exports = router;
