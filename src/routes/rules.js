'use strict';

const express = require('express');
const { rules } = require('../controllers/index');
const authenticateUser = require('../middleware/authenticate-user');

const router = express.Router();

router.get('/', authenticateUser, rules.getRules);
router.get('/:rule_id', authenticateUser, rules.getRule);
router.post('/', authenticateUser, rules.create);
router.put('/:rule_id/enable', authenticateUser, rules.enable);
router.put('/:rule_id/disable', authenticateUser, rules.disable);
router.get('/coin/all', authenticateUser, rules.fetchCoin);

module.exports = router;
