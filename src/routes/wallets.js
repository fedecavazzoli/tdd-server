'use strict';

const express = require('express');
const { wallets } = require('../controllers/index');
const authenticateUser = require('../middleware/authenticate-user');

const router = express.Router();

router.post('/', authenticateUser, wallets.setCredentials);
router.post('/buy', authenticateUser, wallets.buy);
router.post('/sell', authenticateUser, wallets.sell);
router.get('/', authenticateUser, wallets.getWallet);
router.delete('/', authenticateUser, wallets.deleteWallet);

module.exports = router;
