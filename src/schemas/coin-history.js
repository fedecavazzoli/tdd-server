'use strict';

const mongoose = require('mongoose');

const coinHistorySchema = new mongoose.Schema(
	{
		user: mongoose.Schema.Types.ObjectId,
		type: {
			type: String,
			enum: ['BTCUSDT', 'BNBUSDT', 'ETHUSDT'],
			required: [true, 'Type is required']
		},
		value: {
			type: Number,
			required: [true, 'Value is required']
		}
	},
	{
		timestamps: true
	}
);

module.exports = coinHistorySchema;
