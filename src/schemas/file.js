'use strict';

const mongoose = require('mongoose');

const fileSchema = new mongoose.Schema(
	{
		user: mongoose.Schema.Types.ObjectId,
		type: {
			type: String,
			enum: ['file', 'directory'],
			required: [true, 'Type is required']
		},
		name: {
			type: String,
			trim: true,
			lowercase: true,
			required: [true, 'Name is required']
		},
		url: String,
		path: {
			type: String,
			trime: true,
			lowerCase: true,
			required: [true, 'Path is required']
		},
		canShare: {
			type: Boolean,
			default: true
		},
		canRead: {
			type: Boolean,
			default: true
		}
	},
	{
		timestamps: true
	}
);

fileSchema.index({ path: 1, name: 1 }, { unique: true });

module.exports = fileSchema;
