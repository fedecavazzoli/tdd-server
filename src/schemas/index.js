'use strict';

const userSchema = require('./user');
const fileSchema = require('./file');
const shareRequestSchema = require('./share-request');
const ruleSchema = require('./rule');
const coinHistorySchema = require('./coin-history');

module.exports = {
	userSchema,
	fileSchema,
	shareRequestSchema,
	ruleSchema,
	coinHistorySchema
};
