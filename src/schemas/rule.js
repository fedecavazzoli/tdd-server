'use strict';

const mongoose = require('mongoose');

const actionValueSchema = new mongoose.Schema(
	{
		type: { type: String },
		symbol: { type: String },
		name: { type: String },
		arguments: { type: Array }
	}
);

const conditionSchema = new mongoose.Schema(
	{
		type: {
			type: String,
			required: [true, 'type is required']
		},
		name: { type: String },
		value: { type: [Boolean | String | Number] },
		arguments: { type: Array }
	}
);

const actionSchema = new mongoose.Schema(
	{
		type: {
			type: String,
			required: [true, 'type is required']
		},
		name: { type: String },
		symbol: { type: String },
		value: { type: actionValueSchema },
		amount: { type: conditionSchema }
	}
);

const ruleSchema = new mongoose.Schema(
	{
		requiredVariables: { type: Array },
		name: {
			type: String,
			lowercase: true,
			required: [true, 'name is required']
		},
		condition: {
			type: conditionSchema,
			required: [true, 'condition is required']
		},
		action: {
			type: [actionSchema],
			required: [true, 'action is required']
		},
		canShareUsers: [mongoose.Types.ObjectId],
		canReadUsers: [mongoose.Types.ObjectId],
		users: [String],
		enabled: {
			type: Boolean,
			default: true
		}
	}
);

module.exports = ruleSchema;
