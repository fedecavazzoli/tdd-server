'use strict';

const apisauce = require('apisauce');

const BASEURL = 'https://api1.binance.com';

const binanceApi = apisauce.create({
	baseURL: BASEURL,
	timeout: 15000
});

module.exports = {
	binanceApi
};
