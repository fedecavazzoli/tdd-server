'use strict';

const { binanceApi } = require('./binanceApi');

const getKlineData = async ({ symbol, interval }) => {
	const res = await binanceApi.get(
		'/api/v3/klines?symbol=' + symbol + '&interval=' + interval + '&limit=1'
	);
	return res.data[0];
};

const getHighValueByInterval = async parameters => {
	const data = await getKlineData(parameters);
	return data[2];
};

const getLowValueByInterval = async parameters => {
	const data = await getKlineData(parameters);
	return data[3];
};

const getAvgValueByInterval = async parameters => {
	const data = await getKlineData(parameters);
	return (data[3] + data[2]) / 2;
};

module.exports = {
	getHighValueByInterval,
	getLowValueByInterval,
	getAvgValueByInterval
};
