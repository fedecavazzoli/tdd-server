'use strict';

const WebSocket = require('ws');
const events = require('events');
const utils = require('./utils');

const eventsEmitter = new events.EventEmitter();

class Publisher {
	constructor(type) {
		this.type = type;
		this.urlBinanceWSBase = 'wss://stream.binance.com:9443/ws/';
		this.ws = new WebSocket(this.urlBinanceWSBase + utils.setBinanceUrl(this.type));
	}

	publishMessage() {
		this.ws.onmessage = e => {
			const data = JSON.parse(e.data);
			eventsEmitter.emit(this.type, data);
		};
	}
}

module.exports = {
	eventsEmitter,
	Publisher
};
