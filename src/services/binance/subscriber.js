'use strict';

const { eventsEmitter } = require('./publisher');
const coins = require('../coin');
const rules = require('../rules');

// eslint-disable-next-line prefer-const
let coinValues = { BTCUSDT: 0, ETHUSDT: 0, BNBUSDT: 0 };
const regexValue = { BTCUSDT: 'BTC', ETHUSDT: 'ETH', BNBUSDT: 'BNB' };
const defaultValues = { BTCUSDT: 0, ETHUSDT: 0, BNBUSDT: 0 };

const loadHistory = async () => {
	// eslint-disable-next-line no-unused-vars
	Object.keys(coinValues).forEach(async (key, _index) => {

		let lastRegiteredValue = null;

		try {
			lastRegiteredValue = await coins.loadLastValue(key);
		} catch(error) {
			coinValues[key] = defaultValues[key];
		}
		if(!lastRegiteredValue) {
			coinValues[key] = defaultValues[key];
			return;
		}
		coinValues[key] = lastRegiteredValue;
	});
};

const lastValue = type => {
	return coinValues[type];
};

const modifyValue = (type, newValue) => {
	coinValues[type] = newValue;
	return coins.create({ type, value: newValue });
};

const mappedType = type => {
	return regexValue[type];
};

class Subscriber {
	constructor(type) {
		this.type = type;
	}

	async setAsyncData() {

		await loadHistory();

		eventsEmitter.on(this.type, async data => {
			if(Math.abs(lastValue(this.type) - data.k.c) > 0.001 * lastValue(this.type)) {
				console.log(this.type);
				modifyValue(this.type, data.k.c);
				const allRules = await rules.getAllWithCoin(mappedType(this.type));
				allRules.forEach(async rule => { return rules.evaluateRule(rule); });
			}

		});
	}

}

module.exports = {
	Subscriber
};
