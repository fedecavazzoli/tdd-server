'use strict';

/* eslint-disable max-len */
const CryptoJS = require('crypto-js');
const { post } = require('../external_requests');

const baseURL = 'https://testnet.binance.vision';

const buy = async ({ apiKey, apiSecret }, { symbol, amount }) => {

	const date = new Date();
	const timestamp = date.getTime();
	const signature = CryptoJS.HmacSHA256(
		`symbol=${symbol}&side=BUY&type=MARKET&quantity=${amount}&newClientOrderId=my_order_id_3&newOrderRespType=ACK&timestamp=${timestamp}`,
		apiSecret
	).toString(CryptoJS.enc.Hex);

	const response = await post({
		baseURL,
		apiRoute: `/api/v3/order?symbol=${symbol}&side=BUY&type=MARKET&quantity=${amount}&newClientOrderId=my_order_id_3&newOrderRespType=ACK&timestamp=${timestamp}&signature=${signature}`,
		headers: {
			'X-MBX-APIKEY': apiKey
		}
	});
	return response.data;
};
const sell = async ({ apiKey, apiSecret }, { symbol, amount }) => {
	const date = new Date();
	const timestamp = date.getTime();
	const signature = CryptoJS.HmacSHA256(
		`symbol=${symbol}&type=MARKET&quantity=${amount}&newClientOrderId=my_order_id_3&newOrderRespType=ACK&timestamp=${timestamp}&side=SELL`,
		apiSecret
	).toString(CryptoJS.enc.Hex);

	const response = await post({
		baseURL,
		apiRoute: `/api/v3/order?symbol=${symbol}&type=MARKET&quantity=${amount}&newClientOrderId=my_order_id_3&newOrderRespType=ACK&timestamp=${timestamp}&signature=${signature}&side=SELL`,
		headers: {
			'X-MBX-APIKEY': apiKey
		}
	});
	return response.data;
};

const setBinanceUrl = (type, interval = '30m') => (
	type.toLowerCase() + '@kline_' + interval
);


module.exports = {
	setBinanceUrl,
	buy,
	sell
};
