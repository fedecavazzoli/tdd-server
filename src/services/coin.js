'use strict';

const { coinHistorySchema } = require('../schemas/index');
const Model = require('../databases/mongodb/model');
const { secondsTraveler } = require('../helpers/date');

const coinHistoryModel = new Model('CoinHistory', coinHistorySchema);


const create = async coinData => {
	return coinHistoryModel.create(coinData);
};

const loadLastValue = async coin => {
	const { value } = await (await coinHistoryModel.find({ type: coin })).sort({ createdAt: -1 });
	if(!value)
		return null;
	return value;
};

const getValues = async ({ symbol, from, until }) => {
	from = secondsTraveler(from);
	until = secondsTraveler(until);
	const finded = await coinHistoryModel.find({
		type: symbol.toString().replace('/', ''),
		createdAt: {
			$gte: new Date(from.toString()),
			$lte: new Date(until.toString())
		}
	});
	return finded;
};

module.exports = { create, loadLastValue, getValues };
