'use strict';

const axios = require('axios');
require('dotenv').config();

const TIMEOUT = 30000;

const get = ({ baseURL, headers, apiRoute }) => {
	const instance = axios.create({ baseURL, timeout: TIMEOUT });

	return instance.get(apiRoute, { ...headers && { headers } });
};

const post = ({ baseURL, headers, apiRoute }) => {
	const instance = axios.create({ baseURL, timeout: TIMEOUT });
	const body = undefined;
	return instance.post(apiRoute, body, { ...headers && { headers } });
};

module.exports = {
	get,
	post
};
