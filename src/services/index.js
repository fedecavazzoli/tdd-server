'use strict';

const users = require('./users');
const files = require('./files');
const rules = require('./rules');
const TokenServices = require('./token');

module.exports = {
	users,
	files,
	TokenServices,
	rules
};
