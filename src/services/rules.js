'use strict';

const { ObjectId } = require('mongoose').Types;
const { ruler } = require('./rules/condition-evaluator');
const { ruleSchema } = require('../schemas/index');
const Model = require('../databases/mongodb/model');

const ruleModel = new Model('Rules', ruleSchema);

const getAllWithCoin = coin => {
	return ruleModel.find({ enabled: true, requiredVariables: { $in: [new RegExp('.*' + coin + '.*', 'i')] } });
};

const createRule = async ruleData => {
	console.log(ruleData);
	return ruleModel.create(ruleData);
};

const getRuleByName = async name => {
	return ruleModel.findOne('name', name);
};

const evaluateRule = async rule => {
	return ruler(rule);
};

const getByUser = async userId => {
	return ruleModel.find({ users: { $in: [userId] } });
};

const updateEnable = async (ruleId, newState) => {
	return ruleModel.update({ _id: new ObjectId(ruleId) }, { enabled: newState });
};

const getRuleById = async ruleId => {
	return ruleModel.find({ _id: new ObjectId(ruleId) });
};

module.exports = {
	createRule, evaluateRule, getRuleByName, getByUser, updateEnable, getRuleById, getAllWithCoin
};
