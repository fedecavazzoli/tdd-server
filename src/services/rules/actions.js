'use strict';

const users = require('../users');
const binanceUtils = require('../binance/utils');

const buy = async (userId, symbol, amount) => {
	const { wallet } = await users.getUserById(userId);
	return binanceUtils.buy(wallet, { symbol, amount });
};

const sell = async (userId, symbol, amount) => {
	const { wallet } = await users.getUserById(userId);

	return binanceUtils.sell(wallet, { symbol, amount });
};

module.exports = { buy, sell };
