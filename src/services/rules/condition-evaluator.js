
'use strict';

/* eslint-disable no-console */
const { generateOperations, loadUserVariables } = require('./operations');

const actionHandler = async (userId, actions) => {

	const variables = await loadUserVariables(userId);
	const { executeAction } = await generateOperations(variables);
	for(const currentAction of Object.values(actions)) {
		try {
			const result = await executeAction(userId, currentAction);
			console.log(result);
			return result;
		} catch(e) {
			console.log(e.message);
			throw e;
		}
	}
};

const conditionEvaluator = async (userId, conditionArgs) => {
	// eslint-disable-next-line no-unused-vars

	const variables = await loadUserVariables(userId);

	const { executeCondition } = await generateOperations(variables);

	let shouldExecute = false;
	try {
		shouldExecute = await executeCondition(conditionArgs);
		return shouldExecute;
	} catch(err) { console.log(`failed - with exception ${err.message}`); }

	if(!shouldExecute)
		throw new Error('Will not execute');

};

const ruler = async rulesArgs => {
	const { condition, action, users, name } = rulesArgs;
	for(const userId of users) {
		// eslint-disable-next-line no-return-await
		try {
			const result = await conditionEvaluator(userId, condition);
			if(result === true) {
				console.log(`Condition success for ${name}`);
				return actionHandler(userId, action);
			}
			console.log(`Condition failed for ${name}`);
		} catch(e) { console.log(e.message); }
	}
};

module.exports = { ruler };
