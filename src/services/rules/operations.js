/* eslint-disable guard-for-in */
/* eslint-disable no-use-before-define */

'use strict';

const actions = require('./actions');
const users = require('../users');
const coin = require('../coin');

const loadUserVariables = async userId => {
	return users.loadUserVariables(userId);
};

const generateOperations = async userVariables => {

	const buy = async (userId, action) => {

		const { symbol, amount } = action;
		const value = await executeCondition(amount);
		const stripedSymbol = symbol.replace('/', '');

		try {
			const result = await actions.buy(userId, stripedSymbol, value);
			return { data: 'Buy Completed', result };
		} catch(error) { throw new Error(`cannot buy ${symbol} - ${value};\n { ${error} }`); }

	};

	// eslint-disable-next-line no-unused-vars
	const sell = async (userId, action) => {
		const { symbol, amount } = action;
		const value = await executeCondition(amount);
		const stripedSymbol = symbol.replace('/', '');
		try {
			const result = await actions.sell(userId, stripedSymbol, value);
			return { data: 'Sell Completed', result };
		} catch(error) { throw new Error(`cannot buy ${symbol} - ${value};\n { ${error} }`); }

	};

	const setVariable = async (userId, variable) => {
		const { name, value } = variable;
		const { value: newValue } = await executeCondition(value);
		const newVariale = { name, value: newValue };
		return users.setVariable(userId, newVariale);
	};

	const last = async args => {
		const list = [];
		for(let i = 0; i < args.length; i++) {
			const result = await executeCondition(args[i]);
			if(Array.isArray(result)) {
				list[i] = result;
				continue;
			}
			list[i] = [result];
		}
		const lastOne = list[list.length - 1];
		return lastOne[lastOne.length - 1];
	};

	const first = async args => {
		const list = [];
		for(let i = 0; i < args.length; i++) {
			const result = await executeCondition(args[i]);
			if(Array.isArray(result)) {
				list[i] = result;
				continue;
			}
			list[i] = [result];
		}
		const firstOne = list[0];
		return firstOne[0];
	};

	const max = async args => {
		const list = [];
		for(let i = 0; i < args.length; i++) {
			const result = await executeCondition(args[i]);
			if(Array.isArray(result)) {
				list.concat(result);
				continue;
			}
			list.concat([result]);
		}
		const findedMax = Math.max(...list);
		return findedMax;
	};

	const min = async args => {
		const list = [];
		for(let i = 0; i < args.length; i++) {
			const result = await executeCondition(args[i]);
			if(Array.isArray(result)) {
				list.concat(result);
				continue;
			}
			list.concat([result]);
		}
		const findedMin = Math.min(...list);
		return findedMin;
	};

	const add = async args => {
		let accum = await executeCondition(args[0]);
		args.shift();
		for(const currentCondition of args) {
			const executed = await executeCondition(currentCondition);
			accum += executed;
		}
		return accum;
	};

	const substract = async args => {
		let accum = await executeCondition(args[0]);
		args.shift();
		for(const currentCondition of args) {
			const executed = await executeCondition(currentCondition);
			accum -= executed;
		}
		return accum;
	};

	const multiplicate = async args => {
		let result = 1;
		for(const currentCondition of args) {
			const executed = await executeCondition(currentCondition);
			result *= executed;
		}
		return result;
	};

	const divide = async (numerador, denominador) => {
		const numerator = await executeCondition(numerador);
		const denominator = await executeCondition(denominador);
		return numerator / denominator;
	};

	const negate = number => {
		return (-1) * executeCondition(number);
	};

	const average = async args => {
		const amount = args.length;
		const sum = await add(args);
		return sum / amount;
	};

	const and = async args => {
		let anyFalse = true;
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);
			if(result === false)
				anyFalse = false;
		}
		return anyFalse;
	};

	const or = async args => {
		let anyTrue = false;
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);
			if(result === true)
				anyTrue = true;
		}
		return anyTrue;
	};

	const not = async args => {
		const result = await executeCondition(args[0]);
		return !result;
	};

	const equal = async args => {
		let accum = await executeCondition(args[0]);
		let isEqual = true;
		args.shift();
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);
			if(accum !== result)
				isEqual = false;
			accum = result;
		}
		return isEqual;
	};

	const distinct = async args => {
		let accum = await executeCondition(args[0]);
		let isDistinct = true;
		args.shift();
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);
			if(accum === result)
				isDistinct = false;
			accum = result;
		}
		return isDistinct;
	};

	const grater = async args => {
		let accum = await executeCondition(args[0]);
		let isGrater = true;
		args.shift();
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);
			if(accum <= result)
				isGrater = false;
			accum = result;
		}
		return isGrater;
	};

	const graterOrEqual = async args => {
		let accum = await executeCondition(args[0]);
		let isGrater = true;
		args.shift();
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);
			if(accum < result)
				isGrater = false;
			accum = result;
		}
		return isGrater;
	};

	const less = async args => {
		let accum = await executeCondition(args[0]);
		let isLess = true;
		args.shift();
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);

			if(accum >= result)
				isLess = false;
			accum = result;
		}
		return isLess;
	};

	const lessOrEqual = async args => {
		let accum = await executeCondition(args[0]);
		let isLess = true;
		args.shift();
		for(const currentCondition of args) {
			const result = await executeCondition(currentCondition);
			if(accum > result)
				isLess = false;
			accum = result;
		}
		return isLess;
	};

	const exectuteData = async args => {
		const { symbol, from, until, default: defaultParams } = args;
		let values = 0;
		try {
			values = await coin.getValues({ symbol, from, until });
			if(values) {
				const valuesFormatted = values.map(({ value }) => value);
				return valuesFormatted;
			}
		} catch(e) { console.log(e.message); }
		values = [];
		return values.push(executeCondition(defaultParams));
	};

	const executeCall = async call => {
		const { type, name } = call;
		if(type === 'CALL') {
			switch(name) {
				case '*':
					return multiplicate(call.arguments);
				case '+':
					return add(call.arguments);
				case '-':
					return substract(call.arguments);
				case '/':
					return divide(call.arguments[0], call.arguments[1]);
				case 'NEG':
					return negate(call.arguments);
				case 'AVG':
					return average(call.arguments);
				case '=':
					return equal(call.arguments);
				case '>':
					return grater(call.arguments);
				case '<':
					return less(call.arguments);
				case '<=':
					return lessOrEqual(call.arguments);
				case '>=':
					return graterOrEqual(call.arguments);
				case '!=':
					return distinct(call.arguments);
				case 'AND':
					return and(call.arguments);
				case 'OR':
					return or(call.arguments);
				case 'NOT':
					return not(call.arguments);
				case 'LAST':
					return last(call.arguments);
				case 'FIRST':
					return first(call.arguments);
				case 'MAX':
					return max(call.arguments);
				case 'MIN':
					return min(call.arguments);
				default:
					throw new Error(`Non handled type ${name}`);
			}
		}
	};

	const getVariableValue = condition => {
		const { name } = condition;
		const element = Object.values(userVariables).find(variable => variable.name === name);
		if(!element)
			throw new Error(`Cannot find variable ${name}`);
		return element.value;
	};

	const executeCondition = async condition => {
		if(!condition || !condition.type)
			throw new Error('Cannot evaluate condition without type');
		const { type } = condition;
		switch(type) {
			case 'CONSTANT':
				return condition.value;
			case 'VARIABLE':
				return getVariableValue(condition);
			case 'CALL':
				return executeCall(condition);
			case 'DATA':
				return exectuteData(condition);
			default:
				throw new Error(`Non handled condition type ${condition.type}`);
		}
	};

	const executeAction = async (userId, action) => {
		const { type } = action;
		switch(type) {
			case 'BUY_MARKET':
				return buy(userId, action);
			case 'SELL_MARKET':
				return sell(userId, action);
			case 'SET_VARIABLE':
				return setVariable(userId, action);
			default:
				throw new Error(`Non handled action type ${action.type}`);
		}
	};

	// const max = (args) => { result }
	// const min = (args) => { result }
	// const stddev = (args) => { result }

	return { executeCondition, executeAction };

};

module.exports = { generateOperations, loadUserVariables };
