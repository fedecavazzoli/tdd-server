/* eslint-disable no-unused-vars */

'use strict';

const { Subscriber } = require('../services/binance/subscriber');
const { Publisher } = require('../services/binance/publisher');
const { getHighValueByInterval } = require('../services/binance/binanceService');

module.exports = async () => {
	const publisher = new Publisher('BTCUSDT');
	const publisher2 = new Publisher('BNBUSDT');
	const publisher3 = new Publisher('ETHUSDT');
	const subscriber = new Subscriber('BTCUSDT');
	await subscriber.setAsyncData();
	const subscriber2 = new Subscriber('BNBUSDT');
	await subscriber2.setAsyncData();
	const subscriber3 = new Subscriber('ETHUSDT');
	await subscriber3.setAsyncData();

	const config = {
		symbol: 'BTCUSDT',
		interval: '30m'
	};

	publisher.publishMessage();
	publisher2.publishMessage();
	publisher3.publishMessage();

	const res = getHighValueByInterval(config);
};
