
'use strict';

const mongoose = require('mongoose');

module.exports = () => {
	mongoose.connect('mongodb://localhost:27017', {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
		autoIndex: true
	})
		.then(() => console.log('Connected to database'))
		.catch(e => console.log(e));
};
